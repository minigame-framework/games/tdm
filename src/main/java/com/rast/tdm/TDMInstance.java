package com.rast.tdm;

import com.rast.gamecore.GameCore;
import com.rast.gamecore.GameInstance;
import com.rast.gamecore.GameStatus;
import com.rast.gamecore.GameWorld;
import com.rast.gamecore.scores.ScoreFunction;
import com.rast.gamecore.scores.ScoreManager;
import com.rast.gamecore.util.BroadcastWorld;
import com.rast.gamecore.util.CleanPlayer;
import com.rast.gamecore.util.ColorConverter;
import com.rast.gamecore.util.MatchMaker;
import org.bukkit.*;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scoreboard.*;

import java.util.*;

public class TDMInstance extends GameInstance {

    // variables
    private final int maxPlayers;
    private final int minPlayers;
    private final int minKills = 10;

    // bossbars
    private final BossBar bossBar;
    private final BossBar team1goal;
    private final BossBar team2goal;

    // Player groups
    private final Set<Player> waitingPlayers = new HashSet<>();
    private final Set<Player> gamePlayers = new HashSet<>();
    private final Set<Player> respawnPlayers = new HashSet<>();
    private final Set<Player> deadPlayers = new HashSet<>();

    // game flags
    private boolean gameStarted = false;
    private boolean gameEnded = false;
    private boolean pvpEnabled = false;

    // teams
    private final Set<Player> team1players = new HashSet<>();
    private final Set<Player> team2players = new HashSet<>();

    // scoreboard for team1
    private final Scoreboard sb1 = Objects.requireNonNull(Bukkit.getScoreboardManager()).getNewScoreboard();
    private final Objective aheaderObjective = sb1.registerNewObjective("aheader", "dummy", ChatColor.RED + ChatColor.BOLD.toString() + "Team Death Match  ");
    private Score aprevTeam1Kills;
    private Score aprevTeam2Kills;
    private final Team ateam1 = sb1.registerNewTeam("Team1");
    private final Team ateam2 = sb1.registerNewTeam("Team2");
    // scoreboard for team2
    private final Scoreboard sb2 = Objects.requireNonNull(Bukkit.getScoreboardManager()).getNewScoreboard();
    private final Objective bheaderObjective = sb2.registerNewObjective("bheader", "dummy", ChatColor.RED + ChatColor.BOLD.toString() + "Team Death Match  ");
    private Score bprevTeam1Kills;
    private Score bprevTeam2Kills;
    private final Team bteam1 = sb2.registerNewTeam("Team1");
    private final Team bteam2 = sb2.registerNewTeam("Team2");

    // default
    private final Scoreboard defaultSB = Objects.requireNonNull(Bukkit.getScoreboardManager()).getMainScoreboard();

    // team kill count
    private int team1KillCount = 0;
    private int team2KillCount = 0;

    // start timer and grace timer settings
    private long startTimerCount = 0;
    private BukkitTask startCounter;

    // respawn timers
    private final HashMap<Player, RespawnTimer> respawnTimers = new HashMap<>();

    // score tallies
    private final HashMap<Player, Integer> playerKills = new HashMap<>();
    private final HashMap<Player, Integer> playerDeaths = new HashMap<>();

    // despawn timer object
    private BukkitTask worldDespawnTimer;

    // get the variables for the game
    public TDMInstance(GameWorld gameWorld) {
        super(gameWorld);
        maxPlayers = TDM.getSettings().getMapConfig(getGameWorld().getMap()).getMaxPlayers();
        minPlayers = TDM.getSettings().getMapConfig(getGameWorld().getMap()).getMinPlayers();
        bossBar = Bukkit.createBossBar(ChatColor.YELLOW + "Waiting for players...", BarColor.YELLOW, BarStyle.SOLID);
        bossBar.setProgress(1.0);
        // scoreboard stuff
        ChatColor team1color = TDM.getSettings().getMapConfig(getGameWorld().getMap()).getTeam1color();
        ChatColor team2color = TDM.getSettings().getMapConfig(getGameWorld().getMap()).getTeam2color();
        BarColor team1barcolor = ColorConverter.chatToBarColor(team1color);
        BarColor team2barcolor = ColorConverter.chatToBarColor(team2color);
        // team1
        aheaderObjective.setDisplaySlot(DisplaySlot.SIDEBAR);
        Score aspacer1 = aheaderObjective.getScore(ChatColor.GRAY + " -----------------");
        aspacer1.setScore(9);
        Score aspacer2 = aheaderObjective.getScore(ChatColor.GRAY + "                 ");
        aspacer2.setScore(8);
        Score ateam1name = aheaderObjective.getScore(team1color + " | Team 1 | " + ChatColor.GREEN + "← You");
        ateam1name.setScore(7);
        Score ateam2name = aheaderObjective.getScore(team2color + " | Team 2 |");
        ateam2name.setScore(4);
        Score aspacer3 = aheaderObjective.getScore(ChatColor.GRAY + "                  ");
        aspacer3.setScore(2);
        Score aspacer4 = aheaderObjective.getScore(ChatColor.GRAY + " ----------------- ");
        aspacer4.setScore(1);
        Score aspacer5 = aheaderObjective.getScore(ChatColor.GRAY + "             ");
        aspacer5.setScore(5);
        ateam1.setColor(team1color);
        ateam2.setColor(team2color);
        team1goal = Bukkit.createBossBar(ChatColor.GRAY + "Team Power", team2barcolor, BarStyle.SOLID);
        team1goal.setProgress(1.0);
        // team2
        bheaderObjective.setDisplaySlot(DisplaySlot.SIDEBAR);
        Score bspacer1 = bheaderObjective.getScore(ChatColor.GRAY + " -----------------");
        bspacer1.setScore(9);
        Score bspacer2 = bheaderObjective.getScore(ChatColor.GRAY + "                 ");
        bspacer2.setScore(8);
        Score bteam1name = bheaderObjective.getScore(team1color + " | Team 1 |");
        bteam1name.setScore(7);
        Score bteam2name = bheaderObjective.getScore(team2color + " | Team 2 | " + ChatColor.GREEN + "← You");
        bteam2name.setScore(4);
        Score bspacer3 = bheaderObjective.getScore(ChatColor.GRAY + "                  ");
        bspacer3.setScore(2);
        Score bspacer4 = bheaderObjective.getScore(ChatColor.GRAY + " ----------------- ");
        bspacer4.setScore(1);
        Score bspacer5 = bheaderObjective.getScore(ChatColor.GRAY + "             ");
        bspacer5.setScore(5);
        bteam1.setColor(team1color);
        bteam2.setColor(team2color);
        team2goal = Bukkit.createBossBar(null, team1barcolor, BarStyle.SOLID);
        team2goal.setProgress(1.0);
        // for all scoreboards
        team1kills(0, 6);
        team2kills(0, 3);
    }

    // add player
    public void addPlayer(Player player) {
        // first ensure that the game is waiting open and the match does not have the player
        if (getGameWorld().getStatus() == GameStatus.WAITING_OPEN && !hasPlayer(player)) {
            // send the player to the game
            waitPlayerPrep(player);
            bossBar.addPlayer(player);
            BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GRAY + player.getName()
                    + " has joined the match. " + ChatColor.DARK_GRAY + '(' + getPlayerCount() + '/' + maxPlayers + ')');
            startCountdown();
        }
        worldStatusRefresh();
    }

    public void removePlayer(Player player) {
        if (hasPlayer(player)) {
            // remove the tags that may have been assigned to the player
            waitingPlayers.remove(player);
            gamePlayers.remove(player);
            deadPlayers.remove(player);
            respawnPlayers.remove(player);
            bossBar.removePlayer(player);
            team1goal.removePlayer(player);
            team2goal.removePlayer(player);
            player.setScoreboard(defaultSB);
            removePlayerTeams(player);
            BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GRAY + player.getName() + " has left the match.");
            if (!isGameReady()) {
                stopStartCountdown();
            }
            gameWinCheck();
            TDM.getKits().removePlayerKit(player);
            worldStatusRefresh();
            saveScoreTallies(player);
            if (respawnTimers.containsKey(player)) {
                respawnTimers.remove(player).stopTask();
            }
        }
    }

    // prep the player for waiting
    private void waitPlayerPrep(Player player) {
        gamePlayers.remove(player);
        deadPlayers.remove(player);
        waitingPlayers.add(player);
        respawnPlayers.remove(player);
        player.setGameMode(GameMode.ADVENTURE);
        Location loc = TDM.getSettings().getMapConfig(getGameWorld().getMap()).getGameSpawn();
        loc.setWorld(getGameWorld().getBukkitWorld());
        player.teleport(loc);
        CleanPlayer.cleanAll(player);
        player.getInventory().addItem(TDM.getKits().getKitMenuItem());
    }

    // prep the player for the game
    private void gamePlayerPrep(Player player) {
        deadPlayers.remove(player);
        waitingPlayers.remove(player);
        gamePlayers.add(player);
        respawnPlayers.remove(player);
        player.setGameMode(GameMode.SURVIVAL);
        Location loc;
        if (team1players.contains(player)) {
            loc = TDM.getSettings().getMapConfig(getGameWorld().getMap()).getTeam1spawn();
        } else if (team2players.contains(player)) {
            loc = TDM.getSettings().getMapConfig(getGameWorld().getMap()).getTeam2spawn();
        } else {
             loc = TDM.getSettings().getMapConfig(getGameWorld().getMap()).getGameSpawn();
        }
        loc.setWorld(getGameWorld().getBukkitWorld());
        player.teleport(loc);
        CleanPlayer.cleanAll(player);
        TDM.getKits().equipKit(player);
        gameWinCheck();
    }

    // prep the player for the game
    private void respawnPlayerPrep(Player player) {
        deadPlayers.remove(player);
        waitingPlayers.remove(player);
        gamePlayers.remove(player);
        respawnPlayers.add(player);
        player.setGameMode(GameMode.SPECTATOR);
        Location loc = TDM.getSettings().getMapConfig(getGameWorld().getMap()).getGameSpawn();
        loc.setWorld(getGameWorld().getBukkitWorld());
        player.teleport(loc);
        CleanPlayer.cleanAll(player);
        respawnTimers.put(player, new RespawnTimer(player, 5, () -> {
            if (!gameEnded) {
                gamePlayerPrep(player);
            }
        }));
    }

    // prep the player for spectate
    private void spectatePlayerPrep(Player player) {
        gamePlayers.remove(player);
        waitingPlayers.remove(player);
        deadPlayers.add(player);
        respawnPlayers.remove(player);
        player.setGameMode(GameMode.SPECTATOR);
        Location loc = TDM.getSettings().getMapConfig(getGameWorld().getMap()).getGameSpawn();
        loc.setWorld(getGameWorld().getBukkitWorld());
        player.teleport(loc);
        CleanPlayer.cleanAll(player);
    }

    // kill a player
    public void killPlayer(Player player, Player killer) {
        playerDeaths.compute(player, (k, v) -> (v == null) ? 1 : v+1);
        for (PotionEffect effect : player.getActivePotionEffects()) {
            player.removePotionEffect(effect.getType());
        }
        if (killer != null) {
            BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.RED + player.getName() + " has been killed by " + killer.getName() + '!');
            playerKills.compute(killer, (k, v) -> (v == null) ? 1 : v + 1);
        } else {
            BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.RED + player.getName() + " has been killed!");
        }
        if (team2players.contains(player)) {
            team1KillCount++;
            team1kills(team1KillCount, 6);
            for(Player t1p : team1players) {
                t1p.playSound(t1p.getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, SoundCategory.MASTER, Integer.MAX_VALUE, 1.65f);
            }
            for(Player t2p : team2players) {
                t2p.playSound(t2p.getLocation(), Sound.ENTITY_ZOMBIE_DEATH, SoundCategory.MASTER, Integer.MAX_VALUE, 1.65f);
            }
        }
        if (team1players.contains(player)) {
            team2KillCount++;
            team2kills(team2KillCount, 3);
            for(Player t1p : team1players) {
                t1p.playSound(t1p.getLocation(), Sound.ENTITY_ZOMBIE_DEATH, SoundCategory.MASTER, Integer.MAX_VALUE, 1.65f);
            }
            for(Player t2p : team2players) {
                t2p.playSound(t2p.getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, SoundCategory.MASTER, Integer.MAX_VALUE, 1.65f);
            }
        }
        respawnPlayerPrep(player);
        gameWinCheck();
    }

    // check to see if this map has a player
    public boolean hasPlayer(Player player) {
        return waitingPlayers.contains(player) || gamePlayers.contains(player) || deadPlayers.contains(player) || respawnPlayers.contains(player);
    }

    public int getPlayerCount() {
        return waitingPlayers.size() + deadPlayers.size() + gamePlayers.size() + respawnPlayers.size();
    }

    private boolean isGameReady() {
        return minPlayers <= waitingPlayers.size();
    }

    private void startCountdown() {
        // end if game has started or the game is not ready
        if (!isGameReady() || gameStarted) {
            return;
        }
        // do not start the counter if the counter is already running
        if (startCounter != null && !startCounter.isCancelled()) {
            return;
        }
        // we can finally start the counter
        startTimerCount = TDM.getSettings().getGameCountdown();
        BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "There are enough players to start the match.");
        BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_DISPENSER_FAIL, 2);
        startCounter = Bukkit.getScheduler().runTaskTimer(TDM.getPlugin(), () -> {
            if (startTimerCount == 0 && isGameReady()) {
                // timer has reached 0, it is show time
                gameStarted = true;
                worldStatusRefresh();
                bossBar.setVisible(false);
                placePlayersOnTeams();
                // move players into the arena
                Set<Player> playersToMove = new HashSet<>(getWaitingPlayers());
                for (Player playerToMove : playersToMove) {
                    gamePlayerPrep(playerToMove);
                    team1goal.addPlayer(playerToMove);
                    team2goal.addPlayer(playerToMove);
                }
                startGame();
                stopStartCountdown();
                return;
            } else if (startTimerCount == 0) {
                // if the game was not ready and the count is at 0 we want to cancel
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "Countdown was aborted because there was not enough players in the game!");
                bossBar.setTitle(ChatColor.YELLOW + "Waiting for players...");
                bossBar.setProgress(1.0);
                stopStartCountdown();
                return;
            }
            if (startTimerCount == 1) {
                bossBar.setTitle(ChatColor.YELLOW + "Starting game in " + ChatColor.RED + startTimerCount + ChatColor.YELLOW + " second...");
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "Game starting in " + ChatColor.RED + startTimerCount + ChatColor.GOLD + " second.");
                BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_NOTE_BLOCK_BELL, 1);
            } else if (startTimerCount <= 10) {
                bossBar.setTitle(ChatColor.YELLOW + "Starting game in " + ChatColor.RED + startTimerCount + ChatColor.YELLOW + " seconds...");
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "Game starting in " + ChatColor.RED + startTimerCount + ChatColor.GOLD + " seconds.");
                BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_NOTE_BLOCK_BELL, 1);
            } else {
                bossBar.setTitle(ChatColor.YELLOW + "Starting game in " + ChatColor.RED + startTimerCount + ChatColor.YELLOW + " seconds...");
            }
            bossBar.setProgress((double) startTimerCount / 30);
            if (waitingPlayers.size() >= maxPlayers && startTimerCount > 10) {
                startTimerCount = TDM.getSettings().getGameCountdownFast();
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "Game full! Starting match in " + startTimerCount + 's');
                BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_DISPENSER_FAIL, 2);
            } else {
                startTimerCount--;
            }
        }, 0, 20);
    }

    // stop the starter countdown
    private void stopStartCountdown() {
        if (startCounter != null && !startCounter.isCancelled()) {
            startCounter.cancel();
            if (!isGameReady() && !gameStarted) {
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "Countdown was aborted because there was not enough players in the game!");
                BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_DISPENSER_FAIL, 2);
            }
            bossBar.setTitle(ChatColor.YELLOW + "Waiting for players...");
            bossBar.setProgress(1.0);
        }
    }

    // the countdown at the end of the game until the game closes
    private void endGameCountdown() {
        BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.BLUE + "Match is closing in 10 seconds. Use /leave to go back to spawn.");
        if (!TDM.getPlugin().isEnabled()) {
            return;
        }
        Bukkit.getScheduler().runTaskLater(TDM.getPlugin(), () -> TDM.getInstanceManager().purgeInstance(this), 20 * 10L);
    }

    // start the game
    private void startGame() {
        pvpEnabled = true;
    }

    // save player score tallies
    private void saveScoreTallies(Player player) {
        ScoreManager sm = GameCore.getScoreManager();
        if (playerKills.get(player) != null) {
            sm.modifyScore(player, TDM.getTDMGame(), "Kills", playerKills.remove(player), ScoreFunction.ADD);
        }
        if (playerDeaths.get(player) != null) {
            sm.modifyScore(player, TDM.getTDMGame(), "Deaths", playerDeaths.remove(player), ScoreFunction.ADD);
        }
    }

    // check if the win condition has been met. If so end the game.
    private void gameWinCheck() {
        if (!gameStarted) {
            return;
        }

        if (team1players.isEmpty() || team2players.isEmpty()) {
            if (!gameEnded) {
                gameEnded = true;
                if (team2players.isEmpty()){
                    BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GREEN + "" + ChatColor.BOLD + "Team 1 has won the match!");
                    for (Player player : team1players) {
                        if (respawnTimers.containsKey(player)) {
                            respawnTimers.remove(player).stopTask();
                        }
                        GameCore.getScoreManager().modifyScore(player, TDM.getTDMGame(), "Wins", 1, ScoreFunction.ADD);
                        int wins = Math.round(GameCore.getScoreManager().getScore(player, TDM.getTDMGame(), "Wins").get());
                        player.sendMessage(ChatColor.YELLOW + "Your wins have increased by 1");
                        player.sendMessage(ChatColor.GOLD + "You now have " + (wins) + ChatColor.GOLD + " wins!");
                        player.sendMessage(ChatColor.DARK_GRAY + "You had " + ((playerKills.get(player) == null)? 0:playerKills.get(player))
                                + " kills and " + ((playerDeaths.get(player) == null)? 0:playerDeaths.get(player)) + " deaths.");
                        spectatePlayerPrep(player);
                    }
                    for (Player player : team2players) {
                        if (respawnTimers.containsKey(player)) {
                            respawnTimers.remove(player).stopTask();
                        }
                        player.sendMessage(ChatColor.DARK_GRAY + "You had " + ((playerKills.get(player) == null)? 0:playerKills.get(player))
                                + " kills and " + ((playerDeaths.get(player) == null)? 0:playerDeaths.get(player)) + " deaths.");
                        spectatePlayerPrep(player);
                    }
                } else {
                    BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GREEN + "" + ChatColor.BOLD + "Team 2 has won the match!");
                    for (Player player : team2players) {
                        if (respawnTimers.containsKey(player)) {
                            respawnTimers.remove(player).stopTask();
                        }
                        GameCore.getScoreManager().modifyScore(player, TDM.getTDMGame(), "Wins", 1, ScoreFunction.ADD);
                        int wins = Math.round(GameCore.getScoreManager().getScore(player, TDM.getTDMGame(), "Wins").get());
                        player.sendMessage(ChatColor.YELLOW + "Your wins have increased by 1");
                        player.sendMessage(ChatColor.GOLD + "You now have " + (wins) + ChatColor.GOLD + " wins!");
                        player.sendMessage(ChatColor.DARK_GRAY + "You had " + ((playerKills.get(player) == null)? 0:playerKills.get(player))
                                + " kills and " + ((playerDeaths.get(player) == null)? 0:playerDeaths.get(player)) + " deaths.");
                        spectatePlayerPrep(player);
                    }
                    for (Player player : team1players) {
                        if (respawnTimers.containsKey(player)) {
                            respawnTimers.remove(player).stopTask();
                        }
                        player.sendMessage(ChatColor.DARK_GRAY + "You had " + ((playerKills.get(player) == null)? 0:playerKills.get(player))
                                + " kills and " + ((playerDeaths.get(player) == null)? 0:playerDeaths.get(player)) + " deaths.");
                        spectatePlayerPrep(player);
                    }
                }
                endGameCountdown();
                return;
            }
        }

        if (team1KillCount >= minKills || team2KillCount >= minKills) {
            if (!gameEnded) {
                gameEnded = true;
                if (team1KillCount == team2KillCount) {
                    BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "" + ChatColor.BOLD + "Nobody won the match.");
                } else if (team1KillCount > team2KillCount){
                    BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GREEN + "" + ChatColor.BOLD + "Team 1 has won the match!");
                    for (Player player : team1players) {
                        if (respawnTimers.containsKey(player)) {
                            respawnTimers.remove(player).stopTask();
                        }

                        if (GameCore.getScoreManager().isEnabled()) {
                            GameCore.getScoreManager().modifyScore(player, TDM.getTDMGame(), "Wins", 1, ScoreFunction.ADD);
                            int wins = Math.round(GameCore.getScoreManager().getScore(player, TDM.getTDMGame(), "Wins").get());
                            player.sendMessage(ChatColor.YELLOW + "Your wins have increased by 1");
                            player.sendMessage(ChatColor.GOLD + "You now have " + (wins) + ChatColor.GOLD + " wins!");
                        }
                        player.sendMessage(ChatColor.DARK_GRAY + "You had " + ((playerKills.get(player) == null)? 0:playerKills.get(player))
                                + " kills and " + ((playerDeaths.get(player) == null)? 0:playerDeaths.get(player)) + " deaths.");
                        spectatePlayerPrep(player);
                    }
                    for (Player player : team2players) {
                        if (respawnTimers.containsKey(player)) {
                            respawnTimers.remove(player).stopTask();
                        }
                        player.sendMessage(ChatColor.DARK_GRAY + "You had " + ((playerKills.get(player) == null)? 0:playerKills.get(player))
                                + " kills and " + ((playerDeaths.get(player) == null)? 0:playerDeaths.get(player)) + " deaths.");
                        spectatePlayerPrep(player);
                    }
                } else {
                    BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GREEN + "" + ChatColor.BOLD + "Team 2 has won the match!");
                    for (Player player : team2players) {
                        if (respawnTimers.containsKey(player)) {
                            respawnTimers.remove(player).stopTask();
                        }
                        if (GameCore.getScoreManager().isEnabled()) {
                            GameCore.getScoreManager().modifyScore(player, TDM.getTDMGame(), "Wins", 1, ScoreFunction.ADD);
                            int wins = Math.round(GameCore.getScoreManager().getScore(player, TDM.getTDMGame(), "Wins").get());
                            player.sendMessage(ChatColor.YELLOW + "Your wins have increased by 1");
                            player.sendMessage(ChatColor.GOLD + "You now have " + (wins) + ChatColor.GOLD + " wins!");
                        }
                        player.sendMessage(ChatColor.DARK_GRAY + "You had " + ((playerKills.get(player) == null)? 0:playerKills.get(player))
                                + " kills and " + ((playerDeaths.get(player) == null)? 0:playerDeaths.get(player)) + " deaths.");
                        spectatePlayerPrep(player);
                    }
                    for (Player player : team1players) {
                        if (respawnTimers.containsKey(player)) {
                            respawnTimers.remove(player).stopTask();
                        }
                        player.sendMessage(ChatColor.DARK_GRAY + "You had " + ((playerKills.get(player) == null)? 0:playerKills.get(player))
                                + " kills and " + ((playerDeaths.get(player) == null)? 0:playerDeaths.get(player)) + " deaths.");
                        spectatePlayerPrep(player);
                    }
                }
                endGameCountdown();
            }
        }
    }

    // refresh the gameWorld status
    private void worldStatusRefresh() {
        if (gameStarted) {
            getGameWorld().setStatus(GameStatus.RUNNING_CLOSED);
            return;
        }

        if (getPlayerCount() >= maxPlayers) {
            getGameWorld().setStatus(GameStatus.WAITING_CLOSED);
        } else {
            getGameWorld().setStatus(GameStatus.WAITING_OPEN);
        }
        getGameWorld().setPlayerCount(getPlayerCount());
        worldDespawnTimer();
    }

    // start the world despawn time if conditions are met
    private void worldDespawnTimer() {
        if (!TDM.getPlugin().isEnabled()) {
            return;
        }
        if (worldDespawnTimer == null || worldDespawnTimer.isCancelled()) {
            if (getPlayerCount() == 0) {
                worldDespawnTimer = Bukkit.getScheduler().runTaskLater(TDM.getPlugin(), () -> {
                    if (getPlayerCount() == 0) {
                        TDM.getInstanceManager().purgeInstance(this);
                    }
                }, 20 * TDM.getSettings().getWorldDespawnTime());
            }
        } else if (worldDespawnTimer != null && !worldDespawnTimer.isCancelled() && getPlayerCount() != 0) {
            worldDespawnTimer.cancel();
        }
    }

    // getters
    public Set<Player> getWaitingPlayers() {
        return waitingPlayers;
    }

    public Set<Player> getGamePlayers() {
        return gamePlayers;
    }

    public Set<Player> getDeadPlayers() {
        return deadPlayers;
    }

    public Set<Player> getTeam1players() {
        return team1players;
    }

    public Set<Player> getTeam2players() {
        return team2players;
    }

    public boolean pvpEnabled() {
        return pvpEnabled;
    }

    // scoreboard score getters
    public void team1kills(int kills, int index) {
        if (aprevTeam1Kills != null) {
            sb1.resetScores(aprevTeam1Kills.getEntry());
        }
        aprevTeam1Kills = aheaderObjective.getScore(ChatColor.GRAY  + " Team Kills: " + ChatColor.WHITE + kills);
        aprevTeam1Kills.setScore(index);
        if (bprevTeam1Kills != null) {
            sb2.resetScores(bprevTeam1Kills.getEntry());
        }
        bprevTeam1Kills = bheaderObjective.getScore(ChatColor.GRAY  + " Team Kills: " + ChatColor.WHITE + kills);
        bprevTeam1Kills.setScore(index);
        team1goal.setProgress(1 - (team1KillCount/(minKills+0.0)));
    }
    // scoreboard score getters
    public void team2kills(int kills, int index) {
        if (aprevTeam2Kills != null) {
            sb1.resetScores(aprevTeam2Kills.getEntry());
        }
        aprevTeam2Kills = aheaderObjective.getScore(ChatColor.GRAY  + " Team Kills: " + ChatColor.WHITE + kills + ' ');
        aprevTeam2Kills.setScore(index);
        if (bprevTeam2Kills != null) {
            sb2.resetScores(bprevTeam2Kills.getEntry());
        }
        bprevTeam2Kills = bheaderObjective.getScore(ChatColor.GRAY  + " Team Kills: " + ChatColor.WHITE + kills + ' ');
        bprevTeam2Kills.setScore(index);
        team2goal.setProgress(1 - (team2KillCount/(minKills+0.0)));
    }

    // add player to team1
    public void addPlayerTeam1(Player player) {
        team1players.add(player);
        ateam1.addEntry(player.getName());
        bteam1.addEntry(player.getName());
        player.setScoreboard(sb1);
    }

    // add player to team2
    public void addPlayerTeam2(Player player) {
        team2players.add(player);
        ateam2.addEntry(player.getName());
        bteam2.addEntry(player.getName());
        player.setScoreboard(sb2);
    }

    // remove player from all teams
    public void removePlayerTeams(Player player) {
        team1players.remove(player);
        team2players.remove(player);
        ateam1.removeEntry(player.getName());
        ateam2.removeEntry(player.getName());
        bteam1.removeEntry(player.getName());
        bteam2.removeEntry(player.getName());
    }

    // player team chooser
    public void placePlayersOnTeams() {
        // generate the teams
        List<List<Player>> teams = MatchMaker.placePlayersOnTeams(new ArrayList<>(waitingPlayers), 2);

        // now place the players on the teams
        for (Player player : teams.get(0)) {
            addPlayerTeam1(player);
        }
        for (Player player : teams.get(1)) {
            addPlayerTeam2(player);
        }
    }

    public boolean sameTeam(Player player1, Player player2) {
        if (team1players.contains(player1) && team1players.contains(player2)) {
            return true;
        }
        return team2players.contains(player1) && team2players.contains(player2);
    }

}
