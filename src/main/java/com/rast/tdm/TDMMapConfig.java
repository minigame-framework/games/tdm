package com.rast.tdm;

import com.rast.gamecore.MapConfig;
import org.bukkit.ChatColor;
import org.bukkit.Location;

public class TDMMapConfig extends MapConfig {
    private final int minPlayers;
    private final Location team1spawn, team2spawn;
    private final ChatColor team1color, team2color;

    public TDMMapConfig(String name, Location mainSpawn, Location team1spawn, Location team2spawn, ChatColor team1color, ChatColor team2color, int minPlayers, int maxPlayers) {
        super(name, maxPlayers, mainSpawn);
        this.minPlayers = minPlayers;
        this.team1spawn = team1spawn;
        this.team2spawn = team2spawn;
        this.team1color = team1color;
        this.team2color = team2color;
    }

    public int getMinPlayers() {
        return minPlayers;
    }

    public Location getTeam1spawn() {
        return team1spawn;
    }

    public Location getTeam2spawn() {
        return team2spawn;
    }

    public ChatColor getTeam1color() {
        return team1color;
    }

    public ChatColor getTeam2color() {
        return team2color;
    }

}
