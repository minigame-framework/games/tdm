package com.rast.tdm;

import com.rast.gamecore.*;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Team;

import java.util.*;

public class TDMGame extends Game {
    public TDMGame(String name, List<String> mapList, boolean canJoinWhenRunning, JavaPlugin plugin) {
        super(name, mapList, canJoinWhenRunning, plugin);
    }

    @Override
    public String getChatFormat(Player player) {
        Team team1 = player.getScoreboard().getTeam("Team1");
        Team team2 = player.getScoreboard().getTeam("Team2");
        if (team1 != null && team1.hasEntry(player.getName())) {
            return TDM.getSettings().getLocalChatFormat().replace("%color%", team1.getColor().toString());
        } else if (team2 != null && team2.hasEntry(player.getName())) {
            return TDM.getSettings().getLocalChatFormat().replace("%color%", team2.getColor().toString());
        }
        return TDM.getSettings().getLocalChatFormat().replace("%color%", ChatColor.GRAY.toString());
    }

    @Override
    public GameInstance getNewGameInstance(GameWorld gameWorld) {
        return new TDMInstance(gameWorld);
    }
}
