package com.rast.tdm;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.bukkit.entity.Player;

import java.util.Collection;

public class PlayerTags {

    /*
    Reserved for special abilities
     */

    Multimap<Player, String> playerTags = ArrayListMultimap.create();

    public void setTag(Player player, String tag) {
        playerTags.put(player, tag);
    }

    public void removeTag(Player player, String tag) {
        playerTags.remove(player, tag);
    }

    public void removePlayer(Player player) {
        playerTags.removeAll(player);
    }

    public boolean hasTag(Player player, String tag) {
        Collection<String> tags = playerTags.get(player);
        if (tags == null) {
            return false;
        }
        return tags.contains(tag);
    }
}
