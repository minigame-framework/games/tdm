package com.rast.tdm;

import com.rast.gamecore.util.EventProxy;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.block.Container;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class Events extends EventProxy implements Listener {

    // allow players to break tnt.
    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        if (isValid(event.getPlayer(), TDM.getTDMGame().getName())) {
            if (!((TDMInstance) TDM.getInstanceManager().getInstanceFromPlayer(event.getPlayer())).pvpEnabled()) {
                event.setCancelled(true);
                return;
            }
            Material mat =  event.getBlock().getType();
            if (!(mat.equals(Material.TNT) || mat.equals(Material.FIRE))) {
                event.setCancelled(true);
            }
        }
    }

    // prevent players from interacting with containers
    @EventHandler
    public void onPlayerInteractContainer(PlayerInteractEvent event) {
        if (isValid(event.getPlayer(), TDM.getTDMGame().getName())) {
            if (event.getClickedBlock() != null) {
                if (event.getClickedBlock().getState() instanceof Container) {
                    event.setCancelled(true);
                }
            }
        }
    }

    // prevent players from interacting with entities
    @EventHandler
    public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
        if (isValid(event.getPlayer(), TDM.getTDMGame().getName())) {
            event.setCancelled(true);
        }
    }

    // prevent hanging entities from breaking
    @EventHandler
    public void onHangingBreak(HangingBreakEvent event) {
        if (isValid(event.getEntity(), TDM.getTDMGame().getGameSet())) {
            event.setCancelled(true);
        }
    }

    // prevent players from getting items from item frames
    @EventHandler
    public void onDamageEntity(EntityDamageEvent event) {
        if (isValid(event.getEntity(), TDM.getTDMGame().getGameSet())) {
            if (event.getEntityType().equals(EntityType.ITEM_FRAME)) {
                event.setCancelled(true);
            }
        }
    }

    // do not allow explosions to break blocks
    @EventHandler
    public void onEntityExplode(EntityExplodeEvent event) {
        if (isValid(event.getEntity(), TDM.getTDMGame().getGameSet())) {
            event.blockList().clear();
        }
    }

    // only allow tnt and fire to be placed
    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        if (isValid(event.getPlayer(), TDM.getTDMGame().getName())) {
            if (!((TDMInstance) TDM.getInstanceManager().getInstanceFromPlayer(event.getPlayer())).pvpEnabled()) {
                event.setCancelled(true);
                return;
            }
            Material mat =  event.getBlockPlaced().getType();
            if (mat.equals(Material.TNT) && TDM.getSettings().doAutoTNT()) {
                event.getBlockPlaced().setType(Material.AIR);
                event.getBlockPlaced().getWorld().spawnEntity(event.getBlockPlaced().getLocation().add(0.5, 0.5, 0.5), EntityType.PRIMED_TNT);
                event.getBlockPlaced().getWorld().playSound(event.getBlockPlaced().getLocation().add(0.5, 0.5, 0.5), Sound.ENTITY_TNT_PRIMED, SoundCategory.BLOCKS, 1, 1);
            } else if (!(mat.equals(Material.FIRE) || mat.equals(Material.TNT))) {
                event.setCancelled(true);
            }
        }
    }

    // do not allow players to drop the kit menu
    @EventHandler
    public void onDropItem(PlayerDropItemEvent event) {
        if (isValid(event.getPlayer(), TDM.getTDMGame().getName())) {
            event.setCancelled(true);
        }
    }

    // do not allow items to be picked up
    @EventHandler
    public void onPickupItem(EntityPickupItemEvent event) {
        if (isValid(event.getEntity(), TDM.getTDMGame().getGameSet())) {
            event.setCancelled(true);
        }
    }

    // prevent mob eggs before game
    @EventHandler
    public void onCreatureSpawn(CreatureSpawnEvent event) {
        if (isValid(event.getEntity(), TDM.getTDMGame().getGameSet())) {
            if (event.getSpawnReason().equals(CreatureSpawnEvent.SpawnReason.SPAWNER_EGG)) {
                if (!((TDMInstance) TDM.getInstanceManager().getInstanceFromWorld(event.getEntity().getWorld())).pvpEnabled()) {
                    event.setCancelled(true);
                }
            }
        }
    }

    // prevent friendly fire
    @EventHandler (priority = EventPriority.HIGH)
    public void onEntityDamageEntity(EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
            Player player = (Player) event.getEntity();
            if (isValid(player, TDM.getTDMGame().getGameSet(), TDM.getTDMGame().getName())) {
                Player damager = (Player) event.getDamager();
                TDMInstance instance = (TDMInstance) TDM.getInstanceManager().getInstanceFromPlayer(player);
                if ((instance.getTeam1players().contains(player) && instance.getTeam1players().contains(damager)) || (instance.getTeam2players().contains(player) && instance.getTeam2players().contains(damager))) {
                    event.setCancelled(true);
                }
            }
        }
    }

    // stop damage that we do not want
    @EventHandler
    public void onPlayerDamage(EntityDamageEvent event) {
        if (event.getEntity() instanceof Player) {
            Player player = (Player) event.getEntity();
            if (isValid(player, TDM.getTDMGame().getGameSet(), TDM.getTDMGame().getName())) {
                if (!((TDMInstance) TDM.getInstanceManager().getInstanceFromPlayer(player)).pvpEnabled()) {
                    event.setCancelled(true);
                    return;
                }
                if (!((player).getGameMode().equals(GameMode.SURVIVAL))) {
                    event.setCancelled(true);
                }
            }
        }
    }

    // Stop team killing
    @EventHandler
    public void onPlayerDamageByPlayer(EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof Player) {
            Player player = (Player) event.getEntity();
            if (isValid(player, TDM.getTDMGame().getGameSet(), TDM.getTDMGame().getName())) {
                if (event.getDamager() instanceof Player) {
                    if (((TDMInstance) TDM.getInstanceManager().getInstanceFromPlayer(player)).sameTeam(player, (Player) event.getDamager())) {
                        event.setCancelled(true);
                    }
                }
                if (event.getDamager() instanceof Arrow) {
                    Arrow arrow = (Arrow) event.getDamager();
                    if (arrow.getShooter() instanceof Player) {
                        if (((TDMInstance) TDM.getInstanceManager().getInstanceFromPlayer(player)).sameTeam(player, (Player) arrow.getShooter())) {
                            event.setCancelled(true);
                        }
                    }
                }
            }
        }
    }

    // Stop hunger
    @EventHandler
    public void onPlayerHunger(FoodLevelChangeEvent event) {
        if (event.getEntity() instanceof Player) {
            Player player = (Player) event.getEntity();
            if (isValid(player, TDM.getTDMGame().getGameSet(), TDM.getTDMGame().getName())) {
                event.setCancelled(true);
            }
        }
    }

    // Respawn as spectator on death
    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        if (isValid(event.getEntity(), TDM.getTDMGame().getName())) {
            Player killer = event.getEntity().getKiller();
            ((TDMInstance) TDM.getInstanceManager().getInstanceFromPlayer(event.getEntity())).killPlayer(event.getEntity(), killer);
            event.setDeathMessage(null);
            for (ItemStack itemStack : new ArrayList<>(event.getDrops())) {
                event.getDrops().remove(itemStack);
            }
        }
    }

    // remove arrows that land
    @EventHandler
    public void onArrowHit(ProjectileHitEvent event) {
        if (isValid(event.getEntity(), TDM.getTDMGame().getGameSet())) {
            if (event.getEntityType().equals(EntityType.ARROW) || event.getEntityType().equals(EntityType.SPECTRAL_ARROW))
            event.getEntity().remove();
        }
    }

    // GUI event and prevent armour interaction
    @EventHandler
    public void onInventoryInteract(InventoryClickEvent event) {
        if (isValid(event.getWhoClicked(), TDM.getTDMGame().getGameSet())) {
            if (event.getWhoClicked() instanceof Player) {
                Player player = (Player) event.getWhoClicked();
                if (event.getSlotType().equals(InventoryType.SlotType.ARMOR)) {
                    event.setCancelled(true);
                }
            }
        }
    }
}
